# Arnold for Blender #

BtoA is an unofficial Blender add-on for Autodesk's Arnold render engine.

### NOTICE ###
We have moved the BtoA repository [over to GitHub](https://github.com/lunadigital/btoa) to better facilitate community engagement and feedback. This repository will be frozen and archived for posterity's sake, but all new development will happen on the GitHub repo. 